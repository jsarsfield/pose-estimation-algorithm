'''
Random Forest
Code to create, train and test forest of RTW trees
'''
from RTW import RTW
import cv2
import utils
import random
import numpy as np
from numba import jit, vectorize, float32, f4, int32
from PCViz import PCViz
import copy
import math
import multiprocessing as mp
import operator

class RandomForest:
    joint_id_to_name = {
        'Head': 0,
        'Neck': 1,
        'L Shoulder': 2,
        'R Shoulder': 3,
        'L Elbow': 4,
        'R Elbow': 5,
        'L Hand': 6,
        'R Hand': 7,
        'Torso': 8,
        'L Hip': 9,
        'R Hip': 10,
        'L Knee': 11,
        'R Knee': 12,
        'L Foot': 13,
        'R Foot': 14
    }

    #TODO Save trained tree filenames to trained forest file
    def __init__(self, joints, jointTo, jointFrom, test_imgs, test_gt_joints, test_point_clouds):
        self.trees = []  # Store trained trees
        self.numOfFeatures = 1000
        self.bootstrapSamplePercent = 0.7 # percentage of data to randomly sample for tree to train/fit
        self.predictorsPercent = 0.66 # sample two thirds of the predictors at each decision node i.e. find best split from randomly sampled 66% of features at decision node e.g. P IS 66% OF numOfFeatures AT ROOT NODE
        self.jointTo = joints[jointTo] # Joint index we are estimating the position of i.e. walking to
        self.jointFrom = joints[jointFrom] # The Joint index we are walking from
        self.data = {} # Store data for samples used in training e.g. features, UVec, offsets etc
        self.train_imgs = None
        self.test_imgs = test_imgs
        self.test_gt_joints = test_gt_joints
        self.test_point_clouds = test_point_clouds
        self.img = 0  # TODO this is temp variable as we are only using one training img atm
        self.gt_joints = None
        self.num_random_points = None
        self.forest_mse_score = None  # Mean Sqaure Error score of forest i.e. average MSE of all trees in forest
        self.forest_mse_SD = None  # Mean Sqaure Error standard deviation of forest i.e. average MSE SD of all trees in forest
        self.num_cores = 7  # Number of logical processors to do parallel computing



        # Calculate depth features for the set using random offsets
    def produce_samples(self):
        tempPoints = utils.probDistr[np.random.choice(utils.probDistr.shape[0], self.num_random_points*3, replace=False), :]
        self.data["samples"] = [np.empty(shape=[0, 1], dtype="int32"),np.empty(shape=[0, 3]),np.empty(shape=[0, 3])] # "samples" = [img_ind, points, uvecs]
        for i in range(len(self.train_imgs)):
            self.data["samples"][0] = np.append(self.data["samples"][0],np.full((self.num_random_points, 1), i, dtype="int32"))
            self.data["samples"][1] = np.append(self.data["samples"][1],np.concatenate((tempPoints[self.num_random_points*2:(self.num_random_points*2)+int(self.num_random_points/2)]+self.gt_joints[i][0],
                                              tempPoints[self.num_random_points * 2:(self.num_random_points * 2) + int(self.num_random_points / 2)] + self.gt_joints[i][1])), axis=0) # Absolute random points to train, sampled from jointTo & jointFrom
            self.data["samples"][2] = np.append(self.data["samples"][2],np.array(list(map(lambda x: utils.uv_offset_to_joint(self.gt_joints[i][0], x), self.data["samples"][1][-self.num_random_points:]))), axis=0)
        self.num_samples = len(self.data["samples"][0])
        """
        self.data["offsets"] = np.append(tempPoints[np.random.choice(int(len(tempPoints)/2),self.num_random_points)],
                                         tempPoints[np.random.choice(int(len(tempPoints)/2),self.num_random_points)]).reshape((-1,2,3)) #np.concatenate((tempPoints[self.num_random_points*2:(self.num_random_points*2)+int(self.num_random_points/2)]+self.gt_joints[0][0],
                                              #tempPoints[self.num_random_points * 2:(self.num_random_points * 2) + int(self.num_random_points / 2)] + self.gt_joints[0][1])).reshape((-1,2,3)) #tempPoints[:self.num_random_points*2].reshape((-1,2,3)) # Relative offsets
        self.data["points"] = np.concatenate((tempPoints[self.num_random_points*2:(self.num_random_points*2)+int(self.num_random_points/2)]+self.gt_joints[0][0],
                                              tempPoints[self.num_random_points * 2:(self.num_random_points * 2) + int(self.num_random_points / 2)] + self.gt_joints[0][1])) # Absolute random points to train, sampled from jointTo & jointFrom TODO Currently only works with one training image
        self.data["uvecs"] = np.array(list(map(lambda x : utils.uv_offset_to_joint(self.gt_joints[0][0],x), self.data["points"]))) # TODO needs changing to handle multiple training images
        
        #self.data["features"] = np.array([utils.calculate_feature(self.train_imgs[self.img],self.data["points"][ind],self.data["offsets"][ind]) for ind in range(len(self.data["points"]))], dtype="float32") # TODO needs changing to handle multiple training images
        inds = np.argsort(self.data["features"])
        self.data["offsets"] = self.data["offsets"][inds]
        self.data["points"] = self.data["points"][inds]
        self.data["uvecs"] = self.data["uvecs"][inds]
        #self.data["features"] = self.data["features"][inds]
        """
        print("")
        #offsetsRelative = np.array(, dtype="float32")

        # np.random.choice(a, 9, replace=False)
        # np.array([1,2,3],dtype="float32")
        # np.array((1,3), dtype="float32")
        '''
        offsets = [utils.probDistr[random.randint(0, utils.psize)],
                   utils.probDistr[random.randint(0, utils.psize)]]  # Offset points for this split
        for i in range(len(self.set)):
            self.set[i][5] = self.calculate_feature(self.set[i][2], self.set[i][0], offsets)
        self.set = sorted(self.set, key=lambda x: x[5])  # Sort self.set by feature
        features = [x[5] for x in self.set if x[5] != 500]
        median_index = None
        if features:
            median_index = next(i for i, v in enumerate(features) if v >= statistics.median(features))
        if not median_index or median_index < self.node_min or median_index > len(self.set) - self.node_min:
            median_index = int(len(self.set) / 2)  # Not a good set of features so split down the middle
        '''

    def predict_forest(self, img, gt_joint_from, num_steps, num_best_trees=10):
        walk = []  # Store walk Debugging
        walk.append(gt_joint_from)
        for step in range(num_steps):
            predictions = np.empty(shape=(0,3))
            # Get prediction from best (num_best_trees) trees in forest
            for i in range(0, num_best_trees):
                predictions = np.append(predictions, [self.trees[i].predict(img, walk[-1])], axis=0)
            # Average direction vector from all trees
            walk.append(np.mean(predictions, axis=0))
        joint_pos = sum(walk)/num_steps
        return walk[-1], walk


    def train_forest(self, training_depth_imgs, gt_joints, point_cloud, num_random_points = 400, b_cluster=True, numTrees=100):
        """ Train all the trees in the forest, note each tree receives a subset of the samples. """
        pool = mp.Pool(self.num_cores)
        self.train_imgs = training_depth_imgs
        self.gt_joints = gt_joints
        self.point_cloud = point_cloud
        self.num_random_points = num_random_points
        utils.generate_offsets(num_random_points)
        self.produce_samples()   # Produce the samples for training
        self.trees = [RTW(i, [i[y] for y in (np.sort(random.sample(range(self.num_samples), round(self.num_samples * self.bootstrapSamplePercent))) for _ in range(1)) for i in self.data["samples"]], self.train_imgs, utils.probDistr[np.random.choice(utils.probDistr.shape[0], self.num_random_points, replace=False), :]) for i in range(numTrees)]
        print("pool start")
        self.trees = pool.starmap(fit_tree_pool, ((tree, b_cluster, self.predictorsPercent) for tree in self.trees))
        pool.close()
        print("pool finished")


    def evaluate_forest(self, n_imgs_in_eval, num_predictions, num_steps, bEvaluateTrees=True, bEvaluateForest=False):
        """ Evaluate the Mean Square Error of each tree and the forest. """
        eval_imgs = np.random.choice(np.arange(len(self.test_imgs)), size=n_imgs_in_eval)  # Sample indices of test_imgs
        subset_imgs = [self.test_imgs[i] for i in eval_imgs]
        subset_joint_to = [self.test_gt_joints[i][self.jointTo] for i in eval_imgs]
        subset_joint_from = [self.test_gt_joints[i][self.jointFrom] for i in eval_imgs]
        if bEvaluateTrees: # Calculate MSE of each tree individually
            pool = mp.Pool(self.num_cores)
            mse_scores = pool.starmap(evaluate_tree_pool, ((tree, num_predictions, subset_imgs, subset_joint_to, subset_joint_from, self.num_random_points, num_steps) for tree in self.trees))
            pool.close()
            for i in range(len(self.trees)):
                self.trees[i].mse_score = mse_scores[i]
        self.trees.sort(key=operator.attrgetter("mse_score"))
        if bEvaluateForest:
            # Calculate MSE of forest
            print("Evaluating Forest")
            joint_positions = np.empty(shape=(0, 3))  # Store predicted joint position for all predictions
            img = subset_imgs[0]
            start_from = subset_joint_from[0]
            for i in range(num_predictions):  # number of predictions to make
                joint_pos, _ = self.predict_forest(img, start_from, num_steps)
                joint_positions = np.append(joint_positions, [joint_pos], axis=0)
            self.forest_mse_score = ((joint_positions - subset_joint_to[0]) ** 2).mean()
            print('FOREST MSE: ', self.forest_mse_score, ' num_random_points: ', self.num_random_points, "#Trees ", len(self.trees))


    def GetTreeWithLowestMSE(self):
        mse_scores = [self.trees[i].mse_score for i in range(len(self.trees))]
        min_index = np.argmin(mse_scores)
        print("Lowest MSE is Tree ", min_index, " with MSE ", mse_scores[min_index])
        return min_index


    def VisualiseForestWalk(self, num_steps=1024, tree=None):
        if tree is not None:
            winame = "Visualising Tree "+str(tree)+" MSE: "+str(self.trees[tree].mse_score) + " c to stop vis - ESC to finish walk"
        else:
            winame = "Visualising Forest "+" MSE: "+str(self.forest_mse_score)+" #Trees "+str(len(self.trees)) + " c to stop vis - ESC to finish walk"
        print(winame)
        viz = PCViz()
        viz.start(winame)
        cv2.namedWindow(winame, cv2.WINDOW_NORMAL)
        img = np.random.randint(len(self.test_imgs))  # Get random test image
        while True:
            test_img = self.test_imgs[img]
            test_joint_to = self.test_gt_joints[img][self.jointTo]
            test_joint_from = self.test_gt_joints[img][self.jointFrom]
            test_pc = self.test_point_clouds[img]
            if tree: # Visualise prediction and walk of a single tree
                joint_pos, walk = self.trees[tree].predict_tree(test_img, test_joint_from, num_steps)
            else: # Visualise prediction and walk of forest
                joint_pos, walk = self.predict_forest(test_img, test_joint_from, num_steps)
            viz.add_actors(test_pc.astype(np.float32), [test_joint_to,test_joint_from], joint_pos, walk)
            cv_img = copy.deepcopy(cv2.normalize(test_img, test_img, 0, 1, cv2.NORM_MINMAX))
            cv2.circle(cv_img, tuple(utils.point_cloud_to_image(test_joint_to).tolist()), 1, (0, 0, 0), thickness=2)
            cv2.circle(cv_img, tuple(utils.point_cloud_to_image(test_joint_from).tolist()), 1, (0, 0, 0), thickness=2)
            cv2.circle(cv_img, tuple(utils.point_cloud_to_image(joint_pos).tolist()), 4, (50, 50, 50), thickness=2)
            k = None
            for i in range(len(walk) - 1):
                cv2.line(cv_img, tuple(utils.point_cloud_to_image(walk[i]).tolist()),
                         tuple(utils.point_cloud_to_image(walk[i + 1]).tolist()), (255, 255, 255), thickness=1)
                if k != 27:  # Esc key to stop
                    cv2.imshow(winame, cv_img)
                    k = cv2.waitKey(2)
            cv2.imshow(winame, cv_img)
            key = cv2.waitKey(0)
            if key == ord('c'):  # Close
                break
            elif key == 32: # Spacebar = next img
                img = np.random.randint(len(self.test_imgs))  # Get random test image

    def generate_training_set(self, num_random_points):
        for i in range(len(self.train_imgs)):
            offset = i*num_random_points
            for j in range(num_random_points):
                if j<(num_random_points/2):
                    pos = self.gt_joints[i][0] + utils.probDistr[j]
                else:
                    pos = self.gt_joints[i][1] + utils.probDistr[j]
                uv = utils.uv_offset_to_joint(self.gt_joints[i][0],pos) # unit direction vec from random point to gt joint
                self.sets.append([pos, uv, i, [0,0],j+offset,0])

    def save_forest(self,filename):
        pass

    def load_forest(self,filename):
        pass


def fit_tree_pool(tree, b_cluster, predictors_percent):
    return tree.fit(b_cluster, predictors_percent)

def evaluate_tree_pool(tree, num_predictions, img, gt_pos, gt_joint_from, num_random_points, num_steps):
    return tree.evaluate_tree(num_predictions, img, gt_pos, gt_joint_from, num_random_points, num_steps)

@jit(nopython = True)
def normalize_vec(vec):
    norm_= utils.norm(vec)
    return [vec[0] / norm_,vec[1] / norm_,vec[2] / norm_]