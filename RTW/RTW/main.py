import h5py
import numpy as np
import utils
import math
import random
from random_forest import RandomForest

training_samples = [] # [1] Equation (1) in 3.1  S = (I, q, u) where S is a training sample
    
def depth_map_to_image(depth_map, joints=None, joints3d=None):
    """
    Visualise depth image and joint prediction
    """
    img = cv2.normalize(depth_map, depth_map, 0, 1, cv2.NORM_MINMAX)
    img = np.array(img * 255, dtype = np.uint8)
    img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
    img = cv2.applyColorMap(img, cv2.COLORMAP_OCEAN)
    for j in range(15):
        x, y = joints[j, 0], joints[j, 1]
        cv2.circle(img, (x,y), 1, (255,255,255), thickness=2)
        cv2.circle(img, utils.point_cloud_to_image(joints3d[j][0],joints3d[j][1],joints3d[j][2]), 1, (0,255,0), thickness=2)
        if j == 0:
            for k in range(400):
                cv2.circle(img, utils.point_cloud_to_image(joints3d[j][0]+p[k][0],joints3d[j][1]+p[k][1],joints3d[j][2]+p[k][2]), 1, (255,0,0), thickness=1)
        #cv2.putText(img, joint_id_to_name[j], (x+5, y+5), cv2.FONT_HERSHEY_SIMPLEX, 0.3, (255,255,255))
    return img

def calib():
    pass

def create_depth_map_from_pc(pc, bNorm=False):
    img = np.empty([240,320], dtype=np.float32)
    for i in range(240):
        for j in range(320):
            img[i][j] = pc[(i*320)+j][2]
    if bNorm:
        return cv2.normalize(img, img, 0, 1, cv2.NORM_MINMAX)
    else:
        return img

#[1] 3.1 Create a training set. Does each training image have multiple offset points?
def create_training_set(num_train_imgs, num_test_imgs):
    global probDistr
    with open ("dataset.csv", "r") as myfile:
        location=myfile.readlines()
    location=location[0]
    labels = h5py.File(location+'ITOP_Side_Train\\ITOP_side_train_labels\ITOP_side_train_labels\\ITOP_side_train_labels.h5', 'r')
    depth_maps = h5py.File(location+'ITOP_Side_Train\\ITOP_side_train_depth_map\\ITOP_side_train_depth_map\\ITOP_side_train_depth_map.h5', 'r')
    pcr = h5py.File(location+'ITOP_Side_Train\\ITOP_side_train_point_cloud\\ITOP_side_train_point_cloud\\ITOP_side_train_point_cloud.h5', 'r')
    training_images = [] # Store depth images for training
    test_images = []
    gt_joints = [] # Store ground truth (GT) joint and connecting joint for each depth image
    test_gt_joints = []
    point_clouds = [] # Store point cloud data of each training image, used for visualisation.
    test_point_clouds = []
    total_imgs = len(depth_maps["data"])
    train_inds = list(range(0, total_imgs, math.ceil(total_imgs / num_train_imgs)))
    test_inds = random.sample([i for i in range(0, total_imgs) if i not in train_inds], num_test_imgs)
    for ind, i in enumerate(train_inds + test_inds):  # Number of images to add to training_images TODO currently training one image to ensure code is working
        joints3d = labels['real_world_coordinates'][i].astype(np.float32)
        depth_map = depth_maps['data'][i].astype(np.float32).flatten()
        seg_map = labels['segmentation'][i].astype(np.float32).flatten()
        inds = np.where(seg_map==0)
        depth_map[inds] = 0
        depth_map = depth_map.reshape(240,320)
        #pc = pcr['data'][i].astype(np.float32)
        if ind < len(train_inds):
            point_clouds.append(pcr["data"][i])
            gt_joints.append([joints3d[0],joints3d[1]])
            training_images.append(depth_map)
        else:
            test_point_clouds.append(pcr["data"][i])
            test_gt_joints.append([joints3d[0], joints3d[1]])
            test_images.append(depth_map)

    return training_images, gt_joints, point_clouds, test_images, test_gt_joints, test_point_clouds, train_inds, test_inds

 
if __name__ == '__main__':
    n_train_imgs = 300
    n_test_imgs = 50
    n_imgs_in_eval = 50  # Number of test images to use in evaluation

    training_images, gt_joints, point_clouds, test_images, test_gt_joints, test_point_clouds, train_inds, test_inds = create_training_set(n_train_imgs, n_test_imgs)
    rf = RandomForest({"Head": 0, "Torso": 1}, "Head", "Torso",test_images,test_gt_joints, test_point_clouds)
    num_steps = 256
    rf.train_forest(training_images, gt_joints, None,
                   1000, b_cluster=True, numTrees=60)
    rf.evaluate_forest(n_imgs_in_eval, num_predictions=4, num_steps=num_steps)
    rf.VisualiseForestWalk(num_steps=num_steps, tree=rf.GetTreeWithLowestMSE())
    rf.VisualiseForestWalk(num_steps=num_steps)