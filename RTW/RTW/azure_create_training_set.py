import numpy as np
import math
import random
from random import randint
import pandas as pd

size = 1000*3
maxSampling = 0.5 # Max sampling offset in metres from Ground Truth(GT) joint Paper [1] section 3.1
probDistr = np.empty((0,3), dtype=np.float32) # Spherical uniform probability distribution
psize = 0
offsets = None
img = None # Store training image
gt_joints = None # Store GT joint positions for training image. Two GT joint positions per image e.g. Head & Neck
depth_background_num = 100 # if offset pixel lies on background or outside of image then divide by this
num_random_points = 100 # Number of random points to sample from each depth image.

def azureml_main(img_temp, gt_joints_temp):
    global psize, probDistr, img, gt_joints, offsets
    img = img_temp.as_matrix() # DataFrame to numpy array
    gt_joints = gt_joints_temp.as_matrix()
    generate_offsets()
    offsets = [probDistr[random.randint(0, psize)], probDistr[random.randint(0, psize)]]
    training_set = generate_training_set()
    return training_set,

def generate_training_set():
    global probDistr, gt_joints
    features = []  # feature calculated from point3d and unit vector (label).
    labels = []  # unit direction vector from point3d to GT joint position
    for j in range(num_random_points):
        if j < (num_random_points / 2):
            pos = gt_joints[0] + probDistr[j]
        else:
            pos = gt_joints[1] + probDistr[j]
        uv = uv_offset_to_joint(gt_joints[0], pos)  # unit direction vec from random point to gt joint
        feature = calculate_feature(pos)
        features.append(feature) # Add feature to training set
        labels.append([uv.tolist()]) # Add label to training set
    training_set = pd.concat([pd.DataFrame(features),pd.DataFrame(labels)],axis=1)
    training_set.columns = ['Feature', 'Label']
    return training_set

#[1] section 3.2 Eq. 6 Get normalised depth feature from the random point q (point3D) which needs to be transformed to depth map pixel coord.
def calculate_feature(point3D):
    global offsets, img
    depth_pixel = point_cloud_to_image(point3D)   # Depth at random point
    depth = get_depth_at_pixel(depth_pixel)
    offset1abs = point_cloud_to_image(point3D+offsets[0])
    offset2abs = point_cloud_to_image(point3D+offsets[1])
    offset1 = offset1abs-depth_pixel
    offset2 = offset2abs-depth_pixel
    if get_depth_at_pixel(offset1abs) == 0 or depth == 0:
        offset1 = roundv(div2(offset1, depth_background_num))
    else:
        offset1 = roundv(div2(offset1, depth))
    if get_depth_at_pixel(offset2abs) == 0 or depth == 0:
        offset2 = roundv(div2(offset2, depth_background_num))
    else:
        offset2 = roundv(div2(offset2,depth))
    f = get_depth_at_pixel(depth_pixel+offset1)-get_depth_at_pixel(depth_pixel+offset2)
    return f if f != 0 else depth_background_num # depth_background_num is large constant value for features that were on background or off image

#[3] Returns 2d image coords from 3d point
def point_cloud_to_image(vec):
    c = 0.0035       # intrinsic camera calibration parameter MAY DIFFER FOR Kinect V2 0.0038605 UPDATE from Albert "Try using C = 0.0035. There was a slight bug with float16/float32 precision in the dataset code."
    cz = c*vec[2]
    xi = np.round((vec[0]/cz)+160)
    yi = np.round((-(vec[1]/cz))+120)
    return np.array([xi, yi], dtype=np.int32)

def get_offset():
    return probDistr[np.randint(0, psize)]

#[1] section 3.1
def generate_offsets():
    global psize, probDistr
    points = np.random.uniform(-maxSampling,maxSampling,size).astype(np.float32).reshape((int)(size/3),3)
    probDistr = np.empty((0, 3), dtype=np.float32)
    for i in range(points.shape[0]):
        if np.linalg.norm(points[i]) <= maxSampling:
            probDistr=np.vstack((probDistr,points[i]))
    psize = probDistr.shape[0]-1

# [1] 3.1 unit vector from offset point to joint
def uv_offset_to_joint(vec1, vec2):
    dir = sub3(vec1, vec2)
    norm_ = norm(dir)
    return np.array([dir[0] / norm_,dir[1] / norm_,dir[2] / norm_])

def sub3(vec1, vec2):
    """ Calculate the difference of two 3d vectors. """
    return np.array([vec1[0] - vec2[0],vec1[1] - vec2[1], vec1[2] - vec2[2]])

def norm(vec):
    """ Calculate the norm of a 3d vector. """
    return math.sqrt(vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2])

def roundv(vec):
    return np.rint(vec).astype(np.int32)

def div2(vec, scalar):
    """ Divide a 2d vector by a scalar."""
    return np.array([vec[0]/scalar,vec[1]/scalar], dtype=np.float32)

def get_depth_at_pixel(depth_pixel):
    global img
    if depth_pixel[0] > 319 or depth_pixel[1] > 239 or depth_pixel[0] < 0 or depth_pixel[1] < 0: # if pixel is outside bounds return depth 0
            return 0
    return img[depth_pixel[1]][depth_pixel[0]]

if __name__ == "__main__":
    azureml_main(pd.DataFrame(np.loadtxt(open("depth_img.csv", "rb"), delimiter=",")),pd.DataFrame(np.loadtxt(open('gt_joints.csv', "rb"), delimiter=",")))