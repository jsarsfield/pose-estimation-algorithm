# This class is used for testing only

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import math, random
import numpy as np
xs=ys=zs = []
max = 0.5
size = 1000*3
'''
for i in range(1000):
    phi = random.uniform(0,math.pi*2)
    costheta = random.uniform(-1,1)
    u = random.uniform(0,1)

    theta = arccos( costheta )
    r = R * cuberoot( u )

    x = r * sin( theta) * cos( phi )
    y = r * sin( theta) * sin( phi )
    z = r * cos( theta )
    xs.append(x)
    ys.append(x)
    zs.append(x)
'''
points = np.random.uniform(-max,max,size).reshape(size/3,3)
p = np.empty((0,3))
for i in range(points.shape[0]):
    if np.linalg.norm(points[i]) <= max:
        p=np.vstack((p,points[i]))
#np.linalg.norm(x)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

ax.scatter(p[:,0],p[:,1],p[:,2])
plt.gca().set_aspect('equal', adjustable='box')
plt.show()