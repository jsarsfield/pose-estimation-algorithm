"""
Random Tree Walk algorithm.
Fits probability distribution towards ground truth joint using regression tree.
This class is a single Tree, use random_forest.py to generate a forest of these trees.
"""


import utils
import random
import numpy as np
import statistics
from functools import reduce
from random import randint
import math
import copy
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score    # Determine best number of clusters for k-means
from numpy.random import choice     # Select element from list given probability distribution
#import dill
import datetime
from numba import jit, vectorize, float32, f4, int32
import timeit

#TODO optimisations GPU programming [4], parallelism, vectorisation, bagging, forests, cython/numba key components, single precision instead of double?, representation learning [5]
class RTW(object):
      
    def __init__(self, tree_index, samples, train_imgs, offsets, num_of_splits=20, node_min=8, loadfile=None): # If loadfile then load tree object from file
        self.tree = {"nodes": [{"parent": "root", "set": np.arange(len(samples[0]), dtype="int32"), "bIsLeaf": False, "split_val": None, "offsets": None}]}
        self.tree_index = tree_index  # Tree index in forest trees array
        self.node_evaluating = 0 # The node that is currently being evaluated
        self.node_min = node_min # [1] 3.2 Eq. 5 Minimum number of samples that can be in a node
        self.mse_score = None # The trees Mean Squared Error score
        self.samples = samples # [img_ind, points, uvecs] self.bootstrapSamplePercent of forest samples
        self.train_imgs = train_imgs
        self.all_offsets = offsets
        self.num_of_splits = num_of_splits # Number of splits to tree before selecting best. NOTE this is not used when using extremely randomised trees
        '''
        if loadfile:
            if loadfile[-1] == 'C':
                self.tcid = loadfile
            self.load_tree(loadfile)
        '''

    # TODO  Random Forests https://www.stat.berkeley.edu/~breiman/RandomForests/cc_home.htm             st H. Drucker, “Improving Regressors using Boosting Techniques”, 1997. https://en.wikipedia.org/wiki/AdaBoost
    # TODO https://www.researchgate.net/profile/Andy_Liaw/publication/228451484_Classification_and_Regression_by_RandomForest/links/53fb24cc0cf20a45497047ab/Classification-and-Regression-by-RandomForest.pdf
    def fit(self, b_cluster, predictors_percent):
        """ Fit tree by splitting data_points to minimise the overall variance to the ground truth position.
        Randomly sample predictors_percent of splits at decision node and evaluate."""
        print("Fitting tree: ", self.tree_index)
        # Build tree - Find leaf nodes
        while True:
            node_set = self.tree["nodes"][self.node_evaluating]["set"] # indices of samples in this set
            # Determine if leaf node i.e. if minimum number of samples in set or maximum number of leaf nodes is met
            if len(node_set) > self.node_min:
                # Evaluate splits
                best_split_ind = None  # Store set of best split
                lowest_val = math.inf  # Store value to determine best split
                #split_min = int(self.node_min/2) # Ensures potential splits adhere to minimum samples in a node
                #split_max = int(len(node_set)-split_min)
                # Get two random offets
                offsets = self.all_offsets[np.random.choice(self.all_offsets.shape[0], 2, replace=False)]
                # Calculate features.
                features = np.array([utils.calculate_feature(self.train_imgs[self.samples[0][i]],
                                                             self.samples[1][i],
                                                             offsets) for i in node_set], dtype="float32")
                # Randomly generate split parameter
                # TODO verify if this is better than the commented code. Scrutinise feature function as it doesn't include the z component on the 3d point from the walk
                # TODO cont. is this why it moves away from the ground truth when it comes close? investigate
                split_val = np.random.choice(features) # random.uniform(min(features),max(features))
                # Sort samples by features
                inds = np.argsort(features)
                features = features[inds]
                node_set = node_set[inds]
                # Get index to split on
                split_ind = (np.abs(features - split_val)).argmin()
                half_min = int(math.floor(self.node_min/2))
                if split_ind < half_min: split_ind = half_min
                elif split_ind > len(node_set)-half_min: split_ind = len(node_set)-half_min
                # Create partitions
                left_partition = node_set[:split_ind]
                right_partition = node_set[split_ind:]

                """ Evaluate split isn't done in Extremely Randomosed Trees
                np.random.choice(node_set, size=int(math.ceil(len(node_set)*predictors_percent)))
                splits = random.sample(range(split_min, split_max), int(math.ceil((split_max-split_min)*predictors_percent)))
                for split_index in splits:
                    split_val = evaluate_split(data_points[node_indices[:split_index]], data_points[node_indices[split_index:]])
                    if split_val < lowest_val:
                        lowest_val = split_val
                        best_split_ind = split_index
                """
                self.tree["nodes"][self.node_evaluating]["split_val"] = split_val
                self.tree["nodes"][self.node_evaluating]["offsets"] = offsets
                # Create two new nodes
                self.tree["nodes"][self.node_evaluating]["child_left"] = self.create_node(self.node_evaluating, left_partition) # Create Left Partition
                self.tree["nodes"][self.node_evaluating]["child_right"] = self.create_node(self.node_evaluating, right_partition) # Create Right Partition
            else: # Is Leaf node
                self.tree["nodes"][self.node_evaluating]["bIsLeaf"] = True

            self.node_evaluating += 1  # Move to next node
            if len(self.tree["nodes"]) == self.node_evaluating:  # We have finished training/fitting
                break
        # K-Mean clusters at leaf nodes
        if b_cluster:
            self.cluster_leaf_nodes()
        self.train_imgs = None  # Save space when training
        return self


    def predict(self, img, step_point, s_dist=0.005):
        node_ind = 0 # Index of node we are evaluating
        udir_vec = None # Unit direction vec to step
        while True: # Loop until we reach leaf node
            node = self.tree["nodes"][node_ind]
            if node["bIsLeaf"]:  # If leaf node
                c = choice(len(node["probs"]),p=node["probs"])
                #print('std dev: ',np.linalg.norm(np.std(self.tree[tree_ind][2],axis=0)))
                udir_vec = node["rep_vecs"][c]
                break
            else:   # If decision node
                if utils.calculate_feature(img, step_point, node["offsets"]) < node["split_val"]:  # If depth feature is less than split val then split left
                    node_ind = node["child_left"]
                else: # Split right
                    node_ind = node["child_right"]
        # Return step direction
        res = step_point + (udir_vec * s_dist)
        #res.shape = (1,3)
        return res

    def predict_tree(self, img, gt_joint_from, num_steps):
        walk = []  # Store walk Debugging
        walk.append(gt_joint_from)
        step_max, step_min = 0.01, 0.003
        step_dists = np.arange(step_max, step_min, -((step_max - step_min) / num_steps))
        for i in range(num_steps):  # Number of steps to walk through the depth map
            walk.append(self.predict(img, walk[-1], s_dist=step_dists[i]))
        h_steps = int(num_steps/3)
        joint_pos = sum(walk[-h_steps:]) / h_steps
        return walk[-1], walk

    # Create new node in tree
    def create_node(self, parent, indices):
        ind = len(self.tree["nodes"])
        self.tree["nodes"].append({"parent": parent, "set": indices, "bIsLeaf": False})
        return ind


    def argmax(lst):
        return lst.index(max(lst))


    def draw_tree(self):
        pass


    # [1] 3.3 Eq. 9 Score representative vectors given set of clusters
    def score_clusters(self, set, labels, n_clusters):
        variance = 0  # summed variance of the clusters
        for i in range(n_clusters):
            clust_variance = 0
            cluster = np.array(set)[np.where(labels == i)[0]] # Get unit vecs in cluster
            if len(cluster) == 0:
                continue
            c_avg = (1/len(cluster))*np.sum(cluster,axis=0)
            for j in range(len(cluster)):
                clust_variance += utils.norm(cluster[j]-c_avg)**2
            variance += clust_variance/len(cluster)
        return variance/n_clusters


    def score_cluster(self, cluster, c_avg):
        variance = 0  # variance of this cluster
        for j in range(len(cluster)):
            variance += utils.norm(cluster[j] - c_avg) ** 2
        return variance/len(cluster)


    # [1] 3.3 Eq. 11 Calculate the representative vecs for each cluster and probability given size of cluster
    def calculate_representative_vecs(self,set, node_ind, labels=None, n_clusters=None, b_one_vec=False):
        if b_one_vec:
            self.tree["nodes"][node_ind]["rep_vecs"] = [np.array(set[0])]
            self.tree["nodes"][node_ind]["probs"] = [1]
            return
        probs = np.array([]) # Probabilities of representative vec for each cluster
        rep_vecs = [] # Representative vectors for each cluster
        for i in range(n_clusters):
            cluster = set[np.where(labels == i)[0]] # Get unit vecs in cluster
            if len(cluster) == 0: # Ignore empty clusters
                continue
            c_avg = (1/len(cluster))*np.sum(cluster,axis=0) # Get average vec
            rep_vecs.append(c_avg/utils.norm(c_avg))   # Turn into unit vec
            probs = np.append(probs, 1-self.score_cluster(cluster, c_avg))
        #probs = probs**2 # NOTE this is to the power of 2 to favour more accurate rep vecs
        probs = probs/probs.sum()

        max_prob = np.max(probs)
        probs = [1 if max_prob == i else 0 for i in probs]
        new_prob = 1/sum(probs)
        probs = [new_prob if i == 1 else 0for i in probs]

        # Add to tree
        self.tree["nodes"][node_ind]["rep_vecs"] = rep_vecs
        self.tree["nodes"][node_ind]["probs"] = probs


    # [1] 3.3 K-Means clustering at leaf nodes to find representative vectors for each cluster. Consider x-means clustering to find optimal num of clusters
    def cluster_leaf_nodes(self):
        for node_ind, node in enumerate(self.tree["nodes"]): # Loop leaf nodes
            if node["bIsLeaf"]: # If leaf node then cluster
                set = self.samples[2][node["set"]] # [x[1] for x in self.sets if x[3] == self.tree[i][0]] # Get uvec from samples in this leaf node
                best_score = math.inf
                best_labels_nclusters = None # Store best labels and n_clusters for calculating representative vecs
                n = len(set)+1 if len(set) == 2 else len(set)
                for n_clusters in range(2,n): # 2,len(set) # Loop number of clusters to run k-means
                    kmeans = KMeans(n_clusters=n_clusters, algorithm="full", max_iter=80, n_init=8, precompute_distances=True, n_jobs=1) # n_jobs -1 = all logical processors used for n_init
                    kmeans.fit(set)
                    labels = kmeans.labels_
                    #score = silhouette_score(set, labels, metric='cosine') # TODO Decide on silhouette score or (score_cluster) Eq. 9 as scoring metric
                    score = self.score_clusters(set, labels, n_clusters)
                    if score < best_score:  # Store best scoring kmeans
                        #print("Clusters: ",n_clusters," Size: ",len(set)," var: ",score)
                        best_labels_nclusters = [labels,n_clusters]
                        best_score = score
                # Calculate representative vecs for the clusters in this leaf node
                if len(set) == 1:
                    self.calculate_representative_vecs(set, node_ind, b_one_vec=True)
                else:
                    self.calculate_representative_vecs(set, node_ind, best_labels_nclusters[0],best_labels_nclusters[1])

        # TODO Save trained and clustered tree
        '''
        i=datetime.datetime.now()
        self.tcid = "Tree_%s-%s-%s_%s-%s-%s_i%s-p%s-s%s-d%sTC" % (i.day, i.month, i.year, i.hour, i.minute, i.second, len(self.train_imgs), self.num_random_points,self.num_of_splits, self.split_divisions)
        self.save_tree(self.tcid)
        '''

    # TODO [1] 3.2 Eq. 2 objective function to score the performance of a trained tree. We are currently just using MSE of gt pos and predicted pos
    def evaluate_tree(self, num_predictions, test_imgs, all_test_gt_pos, all_test_gt_joint_from, num_random_points, num_steps):
        print("Evaluating Tree "+str(self.tree_index)+"...")
        for j, img in enumerate(test_imgs):
            gt_pos = all_test_gt_pos[j]
            gt_joint_from = all_test_gt_joint_from[j]
            joint_positions = np.empty(shape=(0, 3))  # Store predicted joint position for all predictions
            gt_positions = np.empty(shape=(0, 3))
            for i in range(num_predictions): # number of predictions to make
                joint_pos, _ = self.predict_tree(img, gt_joint_from, num_steps)
                joint_positions = np.append(joint_positions, [joint_pos], axis=0)
                gt_positions = np.append(gt_positions, [gt_pos], axis=0)
        self.mse_score = ((joint_positions - gt_pos) ** 2).mean()
        print('MSE: ', self.mse_score, ' num_random_points: ', num_random_points, " tree: ", self.tree_index)
        return self.mse_score
    '''
    def save_tree(self,filename):
        with open(filename+'.pickle', 'wb') as handle:
            pickle.dump(self, handle, protocol=pickle.HIGHEST_PROTOCOL)

    def load_tree(self,filename):
        with open(filename+'.pickle', 'rb') as handle:
            tmp = pickle.load(handle)
            self.__dict__.update(tmp.__dict__)
    '''

# [1] section 3.2 Eq. 4 Evaluate split
def evaluate_split(uvecs1, uvecs2):
    # uvecs1 =  np.array([np.array(x[1], dtype=np.float32) for x in set1])
    # uvecs2 =  np.array([np.array(x[1], dtype=np.float32) for x in set2])
    uavg1 = (1/len(uvecs1))*np.add.reduce(uvecs1) # Eq. 3
    uavg2 = (1/len(uvecs2))*np.add.reduce(uvecs2)
    uvar1 = calculate_variance(uvecs1,uavg1)
    uvar2 = calculate_variance(uvecs2,uavg2)
    return uvar1+uvar2

@vectorize([float32(float32, float32)], target='parallel',nopython=True)
def uadd(x, y):
    return x + y

@jit(nopython = True)
def calculate_variance(uvecs,uavg):
    sum = 0
    for i in range(len(uvecs)):
        sum += utils.norm(utils.sub3(uvecs[i],uavg))**2
    return sum

@jit(nopython = True)
def sum3(vec1, vec2, result):
    """ Calculate the sum of two 3d vectors. """
    return np.array([vec1[0] + vec2[0],vec1[1] + vec2[1],vec1[2] + vec2[2]])

#https://gist.github.com/ufechner7/98bcd6d9915ff4660a10 numba maths