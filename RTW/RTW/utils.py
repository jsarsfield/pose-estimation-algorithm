import numpy as np
from numba import jit, vectorize, float32, f4, int64
import math

maxSampling = 0.4 # Max sampling offset in metres from Ground Truth(GT) joint Paper [1] section 3.1
probDistr = np.empty((0,3),dtype=np.float32) # Spherical uniform probability distribution 
psize = 0


# [1] section 3.2 Eq. 6 Get normalised depth feature from the random point q (point3D) which needs to be transformed to depth map pixel coord.
def calculate_feature(img, point3D, offsets):
    #depth_pixel = point_cloud_to_image(point3D)  # Depth at random point
    #depth = get_depth_at_pixel(img, depth_pixel)
    depth = point3D[2]
    offset1abs = point_cloud_to_image(point3D + offsets[0])
    offset2abs = point_cloud_to_image(point3D + offsets[1])
    # offset1 = offset1abs - depth_pixel
    # offset2 = offset2abs - depth_pixel
    offset1Depth = None
    offset2Depth = None
    background = 10
    # if depth == 0:
    #    if (get_depth_at_pixel(img, offset1abs == 0)
    if get_depth_at_pixel(img,
                          offset1abs) == 0:  # This check is performed on the pixel offsets before the pixel offset locations are normalised. Tried it on normalised pixel locations and got bad results.
        offset1Depth = background
    else:
        if depth == 0:
            offset1 = roundv(div2(offset1abs, background))
        else:
            offset1 = roundv(div2(offset1abs, depth))
        offset1Depth = get_depth_at_pixel(img,
                                          offset1abs)  # offset1Depth = get_depth_at_pixel(img, depth_pixel + offset1)

    if get_depth_at_pixel(img, offset2abs) == 0:
        offset2Depth = background
    else:
        if depth == 0:
            offset2 = roundv(div2(offset2abs, background))
        else:
            offset2 = roundv(div2(offset2abs, depth))
        offset2Depth = get_depth_at_pixel(img,
                                          offset2abs)  # offset2Depth = get_depth_at_pixel(img, depth_pixel + offset2)
    # f = get_depth_at_pixel(img,depth_pixel+offset1)-get_depth_at_pixel(img,depth_pixel+offset2)
    f = offset1Depth - offset2Depth
    return f


@jit(nopython = True)
def get_depth_at_pixel(img, depth_pixel):
    if depth_pixel[0] > 319 or depth_pixel[1] > 239 or depth_pixel[0] < 0 or depth_pixel[1] < 0: # if pixel is outside bounds return depth 0
            return 0
    return img[depth_pixel[1]][depth_pixel[0]]


#[3] Returns 2d image coords from 3d point
@jit(nopython=True)
def point_cloud_to_image(vec):
    c = 0.0035       # intrinsic camera calibration parameter MAY DIFFER FOR Kinect V2 0.0038605 UPDATE from Albert "Try using C = 0.0035. There was a slight bug with float16/float32 precision in the dataset code."
    cz = c*vec[2]
    xi = np.round((vec[0]/cz)+160)
    yi = np.round((-(vec[1]/cz))+120)
    return np.array([xi, yi],dtype=np.int32)


def get_offset():
    global probDistr
    return probDistr[randint(0,psize)]


#[1] section 3.1
def generate_offsets(num_random_points):
    global psize, probDistr
    size = (num_random_points*6) * 3
    points = np.random.uniform(-maxSampling,maxSampling,size).astype(np.float32).reshape((int)(size/3),3)
    probDistr = np.empty((0,3),dtype=np.float32)
    for i in range(points.shape[0]):
        if np.linalg.norm(points[i]) <= maxSampling:
            probDistr=np.vstack((probDistr,points[i]))
    psize = probDistr.shape[0]-1


# [1] 3.1 unit vector from offset point to joint
@jit(nopython = True)
def uv_offset_to_joint(gtPos, offsetPos):
    dir = sub3(gtPos, offsetPos)
    norm_= norm(dir)
    return np.array([dir[0] / norm_,dir[1] / norm_,dir[2] / norm_])
'''
def uv_offset_to_joint(p,q):
    r = p - q
    return r/np.linalg.norm(r)
'''


@jit(nopython = True)
def sub3(vec1, vec2):
    """ Calculate the difference of two 3d vectors. """
    return np.array([vec1[0] - vec2[0],vec1[1] - vec2[1], vec1[2] - vec2[2]])


@jit(nopython=True)
def norm(vec):
    """ Calculate the norm of a 3d vector. """
    return math.sqrt(vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2])


@jit(nopython=True)
def roundv(vec):
    return np.rint(vec).astype(np.int32)


@jit(nopython = True)
def div2(vec, scalar):
    """ Divide a 2d vector by a scalar."""
    return np.array([vec[0]/scalar,vec[1]/scalar],dtype=np.float32)