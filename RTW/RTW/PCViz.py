'''
Point Cloud Visualisation of random tree walk through point cloud to find joint position
'''
import vtk

class PCViz:
    
    def __init__(self):
        # create a rendering window and renderer
        self.camera = vtk.vtkCamera()
        #self.camera.SetPosition(0, 0,100)
        #self.camera.SetFocalPoint(0, 0, 0)
        self.ren = vtk.vtkRenderer() # Vtk Renderer
        self.ren.SetBackground(.2, .3, .4)
        self.ren.SetActiveCamera(self.camera)
        self.ren.ResetCamera()
        self.renWin = vtk.vtkRenderWindow() # Vtk Render Window
        self.renWin.AddRenderer(self.ren)
        # create a renderwindowinteractor
        self.iren = vtk.vtkRenderWindowInteractor() # Vtk Render Window Interactor
        self.iren.SetRenderWindow(self.renWin)
        # Actor references
        self.point_cloud = None
        self.gt_joints = []
        self.pjoint = None
        self.walk = []
    
    def start(self,winame):
        # enable user interface interactor
        self.iren.Initialize()
        self.renWin.Render()
        self.renWin.SetWindowName(winame)

    def add_actors(self,pc=None,gt_joints=None,pjoint=None,walk=None):
        # Draw point cloud
        self.draw_point_cloud(pc)
        #self.ren.ResetCamera()
        # Draw ground truth joints in black (Sphere)
        for i in range(len(self.gt_joints)):
            self.ren.RemoveActor(self.gt_joints[i].vtkActor)
        self.gt_joints = []
        for i in range(len(gt_joints)):
            self.gt_joints.append(VtkJoint(gt_joints[i]))
            self.ren.AddActor(self.gt_joints[i].vtkActor)
        # Draw predicted joint position in white (Sphere)
        if pjoint is not None and self.pjoint is None:
            self.pjoint = VtkJoint(pjoint,colour=[1,1,1])
            self.ren.AddActor(self.pjoint.vtkActor)
            self.camera.SetFocalPoint(pjoint)
        else:
            self.ren.RemoveActor(self.pjoint.vtkActor)
            self.pjoint = VtkJoint(pjoint,colour=[1,1,1])
            self.ren.AddActor(self.pjoint.vtkActor)
        # Draw tree walk in white (Line)
        for i in range(len(self.walk)):
            self.ren.RemoveActor(self.walk[i].vtkActor)
        self.walk = []
        for i in range(len(walk)-1):
            self.walk.append(VtkLine(walk[i],walk[i+1],colour=[1,1,1]))
            self.ren.AddActor(self.walk[i].vtkActor)
        self.renWin.Render()
        
    def draw_point_cloud(self,pc):
        if self.point_cloud is not None:
            self.ren.RemoveActor(self.point_cloud.vtkActor)
        self.point_cloud = VtkPointCloud()
        for i in range(len(pc)): # Draw point cloud
            self.point_cloud.addPoint([pc[i][0],pc[i][1],pc[i][2]])
        self.ren.AddActor(self.point_cloud.vtkActor)

class VtkPointCloud:

    def __init__(self, zMin=-10.0, zMax=10.0, maxNumPoints=1e6):
        self.maxNumPoints = maxNumPoints
        self.vtkPolyData = vtk.vtkPolyData()
        self.clearPoints()
        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputData(self.vtkPolyData)
        mapper.SetColorModeToDefault()
        mapper.SetScalarRange(zMin, zMax)
        mapper.SetScalarVisibility(1)
        self.vtkActor = vtk.vtkActor()
        self.vtkActor.SetMapper(mapper)
    
    def addPoint(self, point):
        if self.vtkPoints.GetNumberOfPoints() < self.maxNumPoints:
            pointId = self.vtkPoints.InsertNextPoint(point[:])
            self.vtkDepth.InsertNextValue(point[2])
            self.vtkCells.InsertNextCell(1)
            self.vtkCells.InsertCellPoint(pointId)
        else:
            r = random.randint(0, self.maxNumPoints)
            self.vtkPoints.SetPoint(r, point[:])
        self.vtkCells.Modified()
        self.vtkPoints.Modified()
        self.vtkDepth.Modified()

    def clearPoints(self):
        self.vtkPoints = vtk.vtkPoints()
        self.vtkCells = vtk.vtkCellArray()
        self.vtkDepth = vtk.vtkDoubleArray()
        self.vtkDepth.SetName('DepthArray')
        self.vtkPolyData.SetPoints(self.vtkPoints)
        self.vtkPolyData.SetVerts(self.vtkCells)
        self.vtkPolyData.GetPointData().SetScalars(self.vtkDepth)
        self.vtkPolyData.GetPointData().SetActiveScalars('DepthArray')

class VtkJoint:

    def __init__(self, center, radius=0.03, colour=[0,0,0], zMin=-10.0, zMax=10.0, maxNumPoints=1e6):
        self.source = vtk.vtkSphereSource()
        self.source.SetCenter(center[0],center[1],center[2])
        self.source.SetRadius(radius)
        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputConnection(self.source.GetOutputPort())
        mapper.SetColorModeToDefault()
        mapper.SetScalarRange(zMin, zMax)
        mapper.SetScalarVisibility(1)
        self.vtkActor = vtk.vtkActor()
        self.vtkActor.SetMapper(mapper)
        self.vtkActor.GetProperty().SetColor(colour)
        self.vtkActor.GetProperty().SetOpacity(0.5)

class VtkLine:

    def __init__(self,p1,p2,colour=[1,1,1]):
        # create source
        self.source = vtk.vtkLineSource()
        self.source.SetPoint1(p1[0],p1[1],p1[2])
        self.source.SetPoint2(p2[0],p2[1],p2[2])
        # mapper
        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputConnection(self.source.GetOutputPort())
        # actor
        self.vtkActor = vtk.vtkActor()
        self.vtkActor.SetMapper(mapper)
        # color actor
        self.vtkActor.GetProperty().SetColor(colour)