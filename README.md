# README #

[1] RTW for estimating human pose from depth images.
[2] Useful paper & Independent Analysis of RTW
[3] ITOP dataset to train/test.

References
[1] Yub Jung, Ho, et al. "Random tree walk toward instantaneous 3D human pose estimation." Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition. 2015.
[2] Peng, Boya, and Zelun Luo. "Multi-View 3D Pose Estimation from Single Depth Images."
[3] https://www.albert.cm/projects/viewpoint_3d_pose/

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact